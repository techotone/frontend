import { toast } from "react-toastify";
import config from "../config.json";

export function alertOnStatus(status, errorMessage) {
  if (status == 400 || status == 409 || status == 403 || status == 404 || status == 405) {
    toast.error(errorMessage);
  }
}

export function getBaseApi() {
  let api = process.env.REACT_APP_API_URL || config.baseApi;
  return api;
}

export const ConnectionState = Object.freeze({
  WAITING_FOR_KEY_ACK: 1,
  IDLE: 2,
  BUSY: 3,
  NOT_CONNECTED: 4,
  ACK_RECIEVED_SUCCESS: 5,
  ACK_RECIEVED_FAILED: 6,
  TIME_OUT: 7,
});
