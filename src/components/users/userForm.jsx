import React from "react";
import joi from "joi-browser";
import Form from "../common/form";
import { addUser, getUser, updataUser } from "../../services/userService";
import { getRoles } from "./../../services/roleService";
import { getBanks } from "../../services/banksService";

class UserForm extends Form {
  state = {
    data: { Name: "", UserName: "", EMail: "", RoleId: 0, BankId: 0 },
    roles: [],
    banks: [],
    errors: {},
  };

  async componentDidMount() {
    const roles = getRoles();
    let banks = (await getBanks()) || [];

    const id = this.props.match.params.id;
    let user;
    if (id) {
      //edit ,an attempt to update an existing record ,so we must load the exisiting record
      user = await getUser(id);
      if (user) {
        delete user.id;
        delete user.Owner;
        delete user.createdAt;
        delete user.updatedAt;

        this.setState({ data: user, roles, banks });
      }
    } else {
      let data = { ...this.state.data };
      data.RoleId = roles[0].id;
      if (banks && banks.length > 0) data.BankId = banks[0].id;

      this.setState({ data, roles, banks });
    }
  }

  doSubmit = async () => {
    const id = this.props.match.params.id;
    let result;
    if (id) result = await updataUser(id, this.state.data);
    else result = await addUser(this.state.data);
    if (result) this.props.history.push("/users");
  };

  schema = {
    Name: joi.string().required().min(3).label("Name"),
    UserName: joi.string().required().min(4),
    EMail: joi.string().email().required().label("Email"),
    RoleId: joi.number().positive().required().label("Role"),
    BankId: joi.number().positive().required().label("Bank"),
  };

  render() {
    const { banks, roles } = this.state;
    return (
      <React.Fragment>
        <h1 style={{ textAlign: "center" }}>User Form </h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("Name", "Name")}
          {this.renderInput("UserName", "Username")}
          {this.renderInput("EMail", "Email")}
          {this.renderSelect("RoleId", "role", "Role", roles)}
          {this.renderSelect("BankId", "Name", "Bank", banks)}
          {this.renderButton(this.props.match.params.id ? "Update User" : "Add User")}
        </form>
      </React.Fragment>
    );
  }
}

export default UserForm;
