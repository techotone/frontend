import React, { Component } from "react";
import { Link } from "react-router-dom";
import { NavLink } from "react-router-dom";
import { getBanks } from "../../services/banksService";
import { deleteUser, getUSers } from "../../services/userService";
import TableHeader from "../common/tableHeader";
import { getRoles } from "./../../services/roleService";

class Users extends Component {
  state = {
    users: [],
    roles: [],
    banks: [],
  };

  //This system only contain 3 pre fixed roles. so we do not have back end interfaces for the roles

  async componentDidMount() {
    const users = (await getUSers()) || [];
    const banks = (await getBanks()) || [];
    const roles = getRoles();

    this.setState({ users, banks, roles });
  }

  handleDelete = async (user) => {
    const { users } = this.state;
    const originalState = users;
    const filteredUsers = originalState.filter((item) => {
      return item.id !== user.id;
    });

    this.setState({ users: filteredUsers });
    const result = await deleteUser(user.id);
    if (result !== 0) this.setState({ users: originalState });
  };

  render() {
    const { users, banks, roles } = this.state;

    return (
      <div>
        <h1 style={{ textAlign: "center" }}>Users</h1>
        {users.length === 0 && <h2>No users to display</h2>}
        <NavLink to={"/users/new"} className="nav-link">
          Add New
        </NavLink>

        {users.length > 0 && (
          <table className="table">
            <TableHeader colList={["Name", "UserName", "Email", "Role", "Bank"]} />
            <tbody>
              {users &&
                users.map((user) => (
                  <tr key={user.id}>
                    <td>{<Link to={`/users/${user.id}`}>{user.Name}</Link>}</td>
                    <td>{user.UserName}</td>
                    <td>{user.EMail}</td>
                    <td>
                      {
                        roles.find((role) => {
                          return role.id === user.RoleId;
                        }).role
                      }
                    </td>
                    <td>
                      {user.BankId &&
                        banks.find((bank) => {
                          return bank.id === user.BankId;
                        }).Name}
                    </td>
                    <td>
                      <button className="btn btn-danger" onClick={() => this.handleDelete(user)}>
                        Delete
                      </button>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        )}
      </div>
    );
  }
}

export default Users;
