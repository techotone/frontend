import React from "react";
import joi from "joi-browser";
import Form from "../common/form";
import { addVendor, getVendor, updateVendor } from "../../services/vendorService";

class VendorForm extends Form {
  state = {
    data: { Name: "", EMail: "" },
    errors: {},
  };

  async componentDidMount() {
    const id = this.props.match.params.id;
    if (id) {
      //edit ,an attempt to update an existing record ,so we must load the exisiting record
      const vendor = await getVendor(id);
      if (vendor) {
        delete vendor.id;
        delete vendor.createdAt;
        delete vendor.updatedAt;

        this.setState({ data: vendor });
      }
    }
  }

  doSubmit = async () => {
    const id = this.props.match.params.id;
    let result;
    console.log("sdfdf", id);
    if (id) result = await updateVendor(id, this.state.data);
    else result = await addVendor(this.state.data);

    console.log(result);
    if (result) this.props.history.push("/vendors");
  };

  schema = {
    Name: joi.string().required().min(3),
    EMail: joi.string().email().required(),
  };

  render() {
    return (
      <React.Fragment>
        <h1 style={{ textAlign: "center" }}>Vendor Form </h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("Name", "Name")}
          {this.renderInput("EMail", "Email")}
          {this.renderButton(this.props.match.params.id ? "Update Vendor" : "Add Vendor")}
        </form>
      </React.Fragment>
    );
  }
}

export default VendorForm;
