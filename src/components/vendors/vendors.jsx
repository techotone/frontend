import React, { Component } from "react";
import { deleteVendor, getVendor, getVendors } from "../../services/vendorService";
import { NavLink, Link } from "react-router-dom";
import TableHeader from "../common/tableHeader";

class Vendors extends Component {
  state = { vendors: [] };

  async componentDidMount() {
    const vendors = await getVendors();
    if (vendors) this.setState({ vendors });
  }

  handleDelete = async (vendor) => {
    //optimistic update
    const originalState = this.state.vendors;

    const filteredVendors = originalState.filter((item) => {
      return item.id !== vendor.id;
    });

    this.setState({ vendors: filteredVendors }); //set the state immediatly

    //call the back end service
    const result = await deleteVendor(vendor.id);
    if (result !== 0)
      //errornous state so we restore the prev status
      this.setState({ vendors: originalState });
  };

  render() {
    const { vendors } = this.state;
    return (
      <div>
        <h1 style={{ textAlign: "center" }}>Vendors</h1>
        {this.state.vendors.length === 0 && <h2>No vendors to display</h2>}
        <NavLink to={"/vendors/new"} className="nav-link">
          Add New
        </NavLink>

        {this.state.vendors.length > 0 && (
          <table className="table">
            <TableHeader colList={["Name", "Email"]} />
            <tbody>
              {vendors &&
                vendors.map((vendor) => (
                  <tr key={vendor.id}>
                    <td>{<Link to={`/vendors/${vendor.id}`}>{vendor.Name}</Link>}</td>
                    <td>{vendor.EMail}</td>
                    <td>
                      <button className="btn btn-danger" onClick={() => this.handleDelete(vendor)}>
                        Delete
                      </button>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        )}
      </div>
    );
  }
}

export default Vendors;
