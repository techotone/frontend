import React, { Component } from "react";
import { Link, NavLink } from "react-router-dom";
import auth from "../services/authService";

const NavBar = () => {
  const user = auth.getUser();
  let role;
  if (user) {
    role = user.role;
    role = role.toLowerCase();
  }

  return (
    <nav style={{ marginBottom: 20 }} className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <Link className="navbar-brand" to="/">
          Secura
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            {user && (
              <React.Fragment>
                <li className="nav-item">
                  <NavLink className="nav-link active" aria-current="page" to="/injectionReport">
                    Injection Report
                  </NavLink>
                </li>
                {role === "administrator" && (
                  <React.Fragment>
                    <li className="nav-item">
                      <NavLink className="nav-link active" aria-current="page" to="/users">
                        Users
                      </NavLink>
                    </li>
                    <li className="nav-item">
                      <NavLink className="nav-link" to="/banks">
                        Banks
                      </NavLink>
                    </li>
                    <li className="nav-item">
                      <NavLink className="nav-link" to="/vendors">
                        Vendors
                      </NavLink>
                    </li>
                  </React.Fragment>
                )}
                {role === "operator" && (
                  <React.Fragment>
                    <li className="nav-item">
                      <NavLink className="nav-link" to="/devices">
                        Devices
                      </NavLink>
                    </li>
                    <li className="nav-item">
                      <NavLink className="nav-link" to="/keyinjection">
                        Key Injections
                      </NavLink>
                    </li>
                  </React.Fragment>
                )}
                {role === "key manager" && (
                  <li className="nav-item">
                    <NavLink className="nav-link" to="/keys">
                      Keys
                    </NavLink>
                  </li>
                )}

                <li className="nav-item">
                  <NavLink className="nav-link" to="/logout">
                    Logout
                  </NavLink>
                </li>

                <li className="nav-item">
                  <NavLink className="nav-link" to="/">
                    {user.name}
                  </NavLink>
                </li>
              </React.Fragment>
            )}
            {!user && (
              <li className="nav-item">
                <NavLink className="nav-link" to="/login">
                  Login
                </NavLink>
              </li>
            )}
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
