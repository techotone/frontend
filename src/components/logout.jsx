import React, { Component } from "react";
import { deleteToken } from "./../services/authService";

const Logout = (props) => {
  deleteToken();
  props.history.replace("/");
  return null;
};

export default Logout;
