import React from "react";
import joi from "joi-browser";
import Form from "../common/form";
import { addKey, getKey, getKeys, updateKey } from "../../services/keyService";

class KeyForm extends Form {
  state = {
    data: { Component: "", Data: "" },
    comps: [],
    errors: {},
  };

  async componentDidMount() {
    const id = this.props.match.params.id;
    if (id) {
      //edit ,an attempt to update an existing record ,so we must load the exisiting record
      const key = await getKey(id);
      if (key) {
        delete key.id;
        delete key.createdAt;
        delete key.updatedAt;

        this.setState({ data: key });
      }
    } else {
      //set the currently loaded select values in to the data
      const tmpData = { ...this.state.data };
      tmpData.Component = this.validComps[0].comp;
      this.setState({ data: tmpData, comps: this.validComps });
    }
  }

  doSubmit = async () => {
    const id = this.props.match.params.id;
    let result;
    if (id) result = await updateKey(id, this.state.data);
    else result = await addKey(this.state.data);
    if (result) this.props.history.push("/keys");
  };

  schema = {
    Component: joi.string().valid("COMP 1", "COMP 2").required(),
    Data: joi.string().length(32).required(),
  };

  validComps = [
    { id: "COMP 1", comp: "COMP 1" },
    { id: "COMP 2", comp: "COMP 2" },
  ];

  render() {
    return (
      <React.Fragment>
        <h1 style={{ textAlign: "center" }}>Key Form </h1>
        <div className="row">
          <div className="col-5">
            <span className="border border-2">This is the key form</span>
          </div>
          <div className="col-5">
            <form onSubmit={this.handleSubmit}>
              {this.renderInput("Data", "Key Data (HEX)")}
              {this.renderSelect("Component", "comp", "Component Type", this.state.comps)}
              {this.renderButton(this.props.match.params.id ? "Update Key" : "Add Key")}
            </form>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default KeyForm;
