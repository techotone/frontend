import React, { Component } from "react";
import { Link } from "react-router-dom";
import { NavLink } from "react-router-dom";
import { deleteKey, getKeys } from "../../services/keyService";
import TableHeader from "../common/tableHeader";

class Banks extends Component {
  state = { keys: [] };

  async componentDidMount() {
    const keys = (await getKeys()) || [];
    this.setState({ keys });
  }

  handleDelete = async (key) => {
    const { keys } = this.state;
    const originalState = keys;
    const filteredKeys = originalState.filter((item) => {
      return item.id !== key.id;
    });

    this.setState({ keys: filteredKeys });
    const result = await deleteKey(key.id);
    if (result !== 0) this.setState({ keys: originalState });
  };

  render() {
    const { keys } = this.state;

    return (
      <div>
        <h1 style={{ textAlign: "center" }}>Keys</h1>
        {keys.length === 0 && <h2>No keys to display</h2>}
        <NavLink to={"/keys/new"} className="nav-link">
          Add New
        </NavLink>

        {keys.length > 0 && (
          <table className="table">
            <TableHeader colList={["Component", "Encrypted Key"]} />
            <tbody>
              {keys &&
                keys.map((key) => (
                  <tr key={key.id}>
                    <td>{<Link to={`/keys/${key.id}`}>{key.Component}</Link>}</td>
                    <td>{key.Data}</td>
                    <td>
                      <button className="btn btn-danger" onClick={() => this.handleDelete(key)}>
                        Delete
                      </button>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        )}
      </div>
    );
  }
}

export default Banks;
