import React from "react";
import joi from "joi-browser";
import Form from "../common/form";
import { addBank, getBank, updateBank } from "../../services/banksService";
import { update } from "./../../services/http";

class BankForm extends Form {
  state = {
    data: { Name: "", Primary: true },
    errors: {},
  };

  async componentDidMount() {
    const id = this.props.match.params.id;
    if (id) {
      //edit ,an attempt to update an existing record ,so we must load the exisiting record
      const bank = await getBank(id);
      if (bank) {
        delete bank.id;
        delete bank.createdAt;
        delete bank.updatedAt;

        this.setState({ data: bank });
      }
    }
  }

  doSubmit = async () => {
    const id = this.props.match.params.id;
    let result;
    if (id) result = await updateBank(id, this.state.data);
    else result = await addBank(this.state.data);
    if (result) this.props.history.push("/banks");
  };

  schema = {
    Name: joi.string().required().min(3),
    Primary: joi.boolean().required(),
  };

  render() {
    return (
      <React.Fragment>
        <h1 style={{ textAlign: "center" }}>Bank Form </h1>

        <form onSubmit={this.handleSubmit}>
          {this.renderInput("Name", "Name")}
          {this.renderCheckBox("Primary", "Primary ? ")}
          {this.renderButton(this.props.match.params.id ? "Update Bank" : "Add Bank")}
        </form>
      </React.Fragment>
    );
  }
}

export default BankForm;
