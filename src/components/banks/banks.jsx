import React, { Component } from "react";
import { Link } from "react-router-dom";
import { NavLink } from "react-router-dom";
import { deleteBank, getBanks } from "../../services/banksService";
import TableHeader from "../common/tableHeader";

class Banks extends Component {
  state = { banks: [] };

  async componentDidMount() {
    const banks = (await getBanks()) || [];
    this.setState({ banks });
  }

  handleDelete = async (bank) => {
    const { banks } = this.state;
    const originalState = banks;
    const filteredBanks = originalState.filter((item) => {
      return item.id !== bank.id;
    });

    this.setState({ banks: filteredBanks });
    const result = await deleteBank(bank.id);
    if (result !== 0) this.setState({ banks: originalState });
  };

  render() {
    const { banks } = this.state;

    return (
      <div>
        <h1 style={{ textAlign: "center" }}>Banks</h1>
        {banks.length === 0 && <h2>No banks to display</h2>}
        <NavLink to={"/banks/new"} className="nav-link">
          Add New
        </NavLink>

        {banks.length > 0 && (
          <table className="table">
            <TableHeader colList={["Name", "Primary"]} />
            <tbody>
              {banks &&
                banks.map((bank) => (
                  <tr key={bank.id}>
                    <td>{<Link to={`/banks/${bank.id}`}>{bank.Name}</Link>}</td>
                    <td>{bank.Primary ? "Primary" : "Secondory"}</td>
                    <td>
                      <button className="btn btn-danger" onClick={() => this.handleDelete(bank)}>
                        Delete
                      </button>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        )}
      </div>
    );
  }
}

export default Banks;
