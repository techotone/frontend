import React from "react";
import joi from "joi-browser";
import Form from "../common/form";
import { addBank, getBank, updateBank } from "../../services/banksService";
import { addDevice, getDevice, updateDevice } from "../../services/deviceService";
import { getVendors } from "../../services/vendorService";

class DeviceForm extends Form {
  state = {
    data: { SerialNo: "", TerminalID: "", VendorId: 0 },
    errors: {},
    vendors: [],
  };

  async componentDidMount() {
    const vendors = (await getVendors()) || [];
    this.setState({ vendors });
    const id = this.props.match.params.id;
    if (id) {
      //edit ,an attempt to update an existing record ,so we must load the exisiting record
      const devices = await getDevice(id);
      if (devices) {
        delete devices.id;
        delete devices.createdAt;
        delete devices.updatedAt;

        this.setState({ data: devices, vendors });
      }
    } else {
      const data = { ...this.state.data };
      data.VendorId = vendors[0].id;
      this.setState({ data });
    }
  }

  doSubmit = async () => {
    const id = this.props.match.params.id;
    let result;
    if (id) result = await updateDevice(id, this.state.data);
    else {
      const tmp = [];
      tmp.push(this.state.data);
      result = await addDevice(tmp);
    }

    if (result) this.props.history.push("/devices");
  };

  schema = {
    SerialNo: joi.string().required().min(5).label("Serial Number"),
    TerminalID: joi.string().required().length(8).label("Terminal ID"),
    VendorId: joi.number().positive().required().label("Vendor"),
  };

  //thisis a comment
  render() {
    const vendors = [...this.state.vendors];
    vendors.map((vendor) => {
      delete vendor.EMail;
    });

    return (
      <React.Fragment>
        <h1 style={{ textAlign: "center" }}>Device Form </h1>
        <div className="row">
          <div className="col-5" style={{ textAlign: "justify" }}>
            <h4>
              This form allows you to add new devices in to the system. virtually there is no limit
              for number of devices that you can add. afterwords. for key injections these devices
              will be available if they are connected.
            </h4>
          </div>
          <div className="col-5">
            <form onSubmit={this.handleSubmit}>
              {this.renderInput("SerialNo", "Serial No")}
              {this.renderInput("TerminalID", "Terminal ID")}
              {this.renderSelect("VendorId", "Name", "Vendor", vendors)}
              {this.renderButton(this.props.match.params.id ? "Update Device" : "Add Device")}
            </form>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default DeviceForm;
