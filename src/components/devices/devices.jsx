import React, { Component } from "react";
import { Link } from "react-router-dom";
import { NavLink } from "react-router-dom";
import { deleteDevice, getDevices } from "../../services/deviceService";
import { getVendors } from "../../services/vendorService";
import TableHeader from "../common/tableHeader";

class Devices extends Component {
  state = { devices: [], vendors: [] };

  async componentDidMount() {
    const devices = (await getDevices()) || [];
    const vendors = await getVendors();
    this.setState({ devices, vendors });
  }

  handleDelete = async (device) => {
    const { devices } = this.state;
    const originalState = devices;
    const filteredDevices = originalState.filter((item) => {
      return item.id !== device.id;
    });

    this.setState({ devices: filteredDevices });
    const result = await deleteDevice(device.id);
    if (result !== 0) this.setState({ devices: originalState });
  };

  render() {
    const { devices, vendors } = this.state;

    return (
      <div>
        <h1 style={{ textAlign: "center" }}>Devices</h1>
        {this.state.devices.length === 0 && <h2>No devices to display</h2>}
        <NavLink to={"/devices/new"} className="nav-link">
          Add New
        </NavLink>

        {this.state.devices.length > 0 && (
          <table className="table">
            <TableHeader colList={["SerialNo", "TerminalID", "Vendor"]} />
            <tbody>
              {devices &&
                devices.map((device) => (
                  <tr key={device.id}>
                    <td>{<Link to={`/devices/${device.id}`}>{device.SerialNo}</Link>}</td>
                    <td>{device.TerminalID}</td>
                    <td>
                      {
                        vendors.find((item) => {
                          if (item.id === device.VendorId) {
                            console.log(item.Name);
                            return item.Name;
                          }
                        }).Name
                      }
                    </td>
                    <td>
                      <button className="btn btn-danger" onClick={() => this.handleDelete(device)}>
                        Delete
                      </button>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        )}
      </div>
    );
  }
}

export default Devices;
