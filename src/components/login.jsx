import React, { Component } from "react";
import Form from "./common/form";
import joi from "joi-browser";
import { login } from "../services/userService";
import auth from "./../services/authService";
import { setAuthToken } from "../services/http";

class Login extends Form {
  state = { data: { UserName: "", Password: "" }, errors: {} };

  schema = {
    UserName: joi.string().required().min(3),
    Password: joi.string().required().min(8).max(20),
  };

  doSubmit = async () => {
    const { token } = await login(this.state.data);

    if (token)
      //successfull login, so we save the token and redirect the user based on the role
      auth.saveToken(token);
    setAuthToken(token);
    this.props.history.replace("/");
  };

  render() {
    return (
      <React.Fragment>
        <h1 style={{ textAlign: "center" }}>Login </h1>

        <form onSubmit={this.handleSubmit} style={{ marginRight: 300, marginLeft: 300 }}>
          {this.renderInput("UserName", "Username")}
          {this.renderInput("Password", "Password", "password")}
          {this.renderButton("Login")}
        </form>
      </React.Fragment>
    );
  }
}

export default Login;
