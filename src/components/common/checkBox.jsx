import React, { Component } from "react";

const CheckBox = ({ name, label, onChange, value, defChecked }) => {
  return (
    <div className="form-check">
      <input
        className="form-check-input"
        name={name}
        id={name}
        type="checkbox"
        value={value}
        onChange={onChange}
        defaultChecked={defChecked}
      />
      <label className="form-check-label" htmlFor={name}>
        {label}
      </label>
    </div>
  );
};

export default CheckBox;
