import React, { Component } from "react";
import joi from "joi-browser";
import Input from "./Input";
import Select from "./select";
import CheckBox from "./checkBox";

class Form extends Component {
  state = { data: {}, errors: {} };

  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };

    const propScheme = {
      [name]: this.schema[name],
    };

    const { error } = joi.validate(obj, propScheme);
    return error ? error.details[0].message : null;
  };

  //handles the onchage event of every input
  handleChange = ({ currentTarget: input }) => {
    const error = this.validateProperty(input);
    const errors = {};
    if (error) errors[input.name] = error;
    else delete errors[input.name]; //delete error entry if there is any

    const data = { ...this.state.data };

    let dataValue = input.value;
    if (input.type === "checkbox") dataValue = input.checked;

    data[input.name] = dataValue;
    this.setState({ data, errors });
  };

  validate = () => {
    const { error } = joi.validate(this.state.data, this.schema, { abortEarly: false });
    if (!error) return null;

    const errors = {};
    for (let item of error.details) errors[item.path[0]] = item.message;
    return errors;
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const errors = this.validate();

    if (errors) {
      //set the error
      this.setState({ errors });
      return;
    }

    this.doSubmit();
  };

  renderInput = (name, label, type = "text") => {
    const { data, errors } = this.state;

    return (
      <Input
        name={name}
        type={type}
        label={label}
        value={data[name]}
        error={errors[name]}
        onChange={this.handleChange}
      />
    );
  };

  renderCheckBox = (name, label) => {
    const { data } = this.state;

    return (
      <CheckBox
        name={name}
        label={label}
        value={data[name]}
        onChange={this.handleChange}
        defChecked={data[name]}
      />
    );
  };

  renderSelect = (name, dataLabel, label, options) => {
    const { data, errors } = this.state;

    return (
      <Select
        name={name}
        label={label}
        options={options}
        onChange={this.handleChange}
        dataLabel={dataLabel}
        value={data[name]}
        error={errors[name]}
      />
    );
  };

  renderButton = (label) => {
    return (
      <div className="mb-2">
        <button
          style={{ marginTop: 10, alignSelf: "center" }}
          type="submit"
          className="btn btn-primary"
        >
          {label}
        </button>
      </div>
    );
  };
}

export default Form;
