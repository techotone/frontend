import React, { Component } from "react";

const TableHeader = ({ colList }) => {
  return (
    <thead>
      <tr>
        {colList.map((item) => (
          <th key={item} scope="col">
            {" "}
            {item}
          </th>
        ))}
      </tr>
    </thead>
  );
};

export default TableHeader;
