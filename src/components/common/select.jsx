import React, { Component } from "react";

const Select = ({ name, label, options, value, dataLabel, onChange, error }) => {
  return (
    <div className="mb-3">
      <label htmlFor={name} className="form-label">
        {label}
      </label>
      <select
        onChange={onChange}
        name={name}
        id={name}
        className="form-select"
        aria-label="Default select example"
        value={value}
      >
        {options.map((item) => (
          <option key={item.id} value={item.id}>
            {item[dataLabel]}
          </option>
        ))}
      </select>
      {error && <div className="alert alert-danger">{error}</div>}
    </div>
  );
};

export default Select;
