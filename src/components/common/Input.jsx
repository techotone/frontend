import React, { Component } from "react";

//abstraction of rendering of theinput controller
const Input = ({ name, label, error, value, onChange, type = "text" }) => {
  return (
    <div className="mb-3">
      <label htmlFor={name} className="form-label">
        {label}
      </label>
      <input
        name={name}
        type={type}
        className="form-control"
        value={value}
        id={name}
        onChange={onChange}
        autoFocus
      />
      {error && <div className="alert alert-danger">{error}</div>}
    </div>
  );
};

export default Input;
