import React, { Component } from "react";
import { getDevices } from "../../services/deviceService";
import { getKeyInjections } from "../../services/keyInjectionService";
import { getUSers } from "../../services/userService";
import TableHeader from "../common/tableHeader";

class KeyInjectionReport extends Component {
  state = { injections: [], users: [], devices: [] };

  async componentDidMount() {
    const injections = (await getKeyInjections()) || [];
    const users = (await getUSers()) || [];
    const devices = (await getDevices()) || [];

    this.setState({ injections, users, devices });
  }

  render() {
    const { injections, users, devices } = this.state;

    return (
      <div>
        <h1 style={{ textAlign: "center" }}>Key Injection Report</h1>
        {injections.length === 0 && <h2>No key injections yet</h2>}

        {injections.length > 0 && (
          <table className="table">
            <TableHeader colList={["Injection Status", "Date", "User", "Device Serial No"]} />
            <tbody>
              {injections &&
                injections.map((injection) => (
                  <tr key={injection.id}>
                    <td>{injection.InjectionState}</td>
                    <td>{injection.createdAt}</td>
                    <td>
                      {
                        users.find((user) => {
                          return user.id == injection.UserId;
                        }).Name
                      }
                    </td>

                    <td>
                      {
                        devices.find((device) => {
                          return device.id == injection.DeviceId;
                        }).SerialNo
                      }
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        )}
      </div>
    );
  }
}

export default KeyInjectionReport;
