import React, { Component } from "react";
import { Link } from "react-router-dom";
import { NavLink } from "react-router-dom";
import { getConnectedDevices, injectKeys } from "../../services/keyInjectionService";
import { getVendors } from "../../services/vendorService";
import CheckBox from "../common/checkBox";
import TableHeader from "../common/tableHeader";
import { toast } from "react-toastify";
import { ConnectionState } from "../../utils/utils";

class KeyInjections extends Component {
  state = { devices: [], vendors: [], loading: false };

  async componentDidMount() {
    let devices = (await getConnectedDevices()) || [];
    const vendors = (await getVendors()) || [];

    //insert a key injection invoke status bolean
    devices.map((item) => {
      item.shouldKeyInject = false;
    });

    this.setState({ devices, vendors });
  }

  handleKeyInjection = async (device) => {};

  handleKeyInjectionStatusChange = async (e, device) => {
    const { currentTarget: input } = e;

    const tmpDevices = [...this.state.devices];
    const index = tmpDevices.indexOf(device);

    if (index > -1) {
      const tmpDevice = tmpDevices[index];
      tmpDevice.shouldKeyInject = input.checked;
      this.setState({ devices: tmpDevices });
    }
  };

  handleInjectSelected = async () => {
    let devices = [...this.state.devices];

    if (this.state.loading) return;
    devices = devices.filter((dev) => {
      return dev.shouldKeyInject == true;
    });

    if (devices.length == 0) {
      toast.warn("No device is selected for injection");
      return;
    }

    this.setState({ loading: true });
    const data = await injectKeys(devices);
    this.setState({ loading: false });

    console.log(data);
    if (data) this.setState({ devices: data });
  };

  setRowColor = (device) => {
    if (device.state) {
      const { state } = device;

      if (state == ConnectionState.ACK_RECIEVED_SUCCESS) return "Highlight";
      else return "ActiveBorder";
    }
  };

  setInjectionStatus = (device) => {
    let str = "";

    if (device.state) {
      const { state } = device;
      if (state == ConnectionState.ACK_RECIEVED_SUCCESS) str = "Injection Success!";
      else if (state == ConnectionState.TIME_OUT) str = "Injection Timeout!";
      else if (state == ConnectionState.ACK_RECIEVED_FAILED) str = "Injection Failed!";
      else if (state == ConnectionState.NOT_CONNECTED) str = "Terminal Disconnected!";
      else str = "Unknown Error!";
    }

    return <h4>{str} </h4>;
  };

  render() {
    const { devices, vendors } = this.state;

    return (
      <div>
        <h1 style={{ textAlign: "center" }}>Connected Devices</h1>
        {this.state.loading && <h4 style={{ textAlign: "center" }}>Please wait...</h4>}
        {this.state.devices.length === 0 && <h2>No connected devices to display</h2>}

        {this.state.devices.length > 0 && (
          <React.Fragment>
            <button
              className="btn btn-danger"
              style={{ alignmentBaseline: "after-edge" }}
              onClick={this.handleInjectSelected}
            >
              Inject Selected
            </button>

            <table className="table">
              <TableHeader colList={["Serial No", "Terminal ID", "Vendor", "Injection Status"]} />
              <tbody>
                {devices &&
                  devices.map((device) => (
                    <tr
                      key={device.id}
                      // style={{
                      //   backgroundColor: this.setRowColor(device),
                      // }}
                    >
                      <td>{device.SerialNo}</td>
                      <td>{device.TerminalID}</td>
                      <td>
                        {
                          vendors.find((item) => {
                            return item.id == device.VendorId;
                          }).Name
                        }
                      </td>
                      <td>
                        <CheckBox
                          name="shouldKeyInject"
                          value={devices["shouldKeyInject"]}
                          defChecked={false}
                          onChange={(e) => this.handleKeyInjectionStatusChange(e, device)}
                        />
                      </td>
                      <td>{this.setInjectionStatus(device)}</td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </React.Fragment>
        )}
      </div>
    );
  }
}

export default KeyInjections;
