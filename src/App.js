import React from "react";
import { Redirect } from "react-router-dom";
import { Route, Switch } from "react-router-dom";

import "./App.css";
import Banks from "./components/banks/banks";
import BankForm from "./components/banks/bankForm";
import Vendors from "./components/vendors/vendors";
import VendorForm from "./components/vendors/vendorForm";
import Devices from "./components/devices/devices";
import DeviceForm from "./components/devices/deviceForm";
import NavBar from "./components/navBar";
import Users from "./components/users/users";
import UserForm from "./components/users/userForm";
import Login from "./components/login";
import auth from "./services/authService";
import Keys from "./components/keys/keys";
import KeyForm from "./components/keys/keyForm";
import Logout from "./components/logout";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import KeyInjections from "./components/keyInjections/keyInjectins";
import KeyInjectionReport from "./components/keyInjections/keyInjectionReport";

function App() {
  const user = auth.getUser();
  return (
    <React.Fragment>
      <ToastContainer />
      <NavBar />
      <main className="container">
        {!user && <Redirect from="/" to="/login"></Redirect>}
        <Switch>
          <Route path="/banks/new" component={BankForm}></Route>
          <Route path="/banks/:id" component={BankForm}></Route>
          <Route path="/banks" component={Banks}></Route>
          ``
          <Route path="/vendors/new" component={VendorForm}></Route>
          <Route path="/vendors/:id" component={VendorForm}></Route>
          <Route path="/vendors" component={Vendors}></Route>
          <Route path="/devices/new" component={DeviceForm}></Route>
          <Route path="/devices/:id" component={DeviceForm}></Route>
          <Route path="/devices" component={Devices}></Route>
          <Route path="/users/new" component={UserForm}></Route>
          <Route path="/users/:id" component={UserForm}></Route>
          <Route path="/users" component={Users}></Route>
          <Route path="/keys/new" component={KeyForm}></Route>
          <Route path="/keys/:id" component={KeyForm}></Route>
          <Route path="/keys" component={Keys}></Route>
          <Route path="/keyinjection" component={KeyInjections}></Route>
          <Route path="/injectionReport" component={KeyInjectionReport}></Route>
          <Route path="/login" component={Login}></Route>
          <Route path="/logout" component={Logout}></Route>
          {!user && <Redirect from="/" to="/login"></Redirect>}
          {user && user.role === "Administrator" && <Redirect from="/" to="/users"></Redirect>}
          {user && user.role === "Key Manager" && <Redirect from="/" to="/keys"></Redirect>}
          {user && user.role === "Operator" && <Redirect from="/" to="/devices"></Redirect>}
          <Redirect from="/" to="/banks"></Redirect>
        </Switch>
      </main>
    </React.Fragment>
  );
}

export default App;
