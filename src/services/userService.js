import http from "./http";

import config from "../config.json";
import { alertOnStatus, getBaseApi } from "./../utils/utils";

const baseApi = getBaseApi();
const apiEndPoint = baseApi + "/user/";
const loginEnPoint = baseApi + "/login/";

export async function getUSers() {
  try {
    const { data } = await http.get(apiEndPoint);
    return data;
  } catch (ex) {
    const { status } = ex.response;
    alertOnStatus(status, ex.response.data);
    return null;
  }
}

export async function deleteUser(id) {
  try {
    await http.remove(apiEndPoint + id);
    return 0;
  } catch (ex) {
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
    }
    return 1;
  }
}

export async function addUser(user) {
  try {
    const { data } = await http.add(apiEndPoint, user);
    return data;
  } catch (ex) {
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
    }
    return null;
  }
}

export async function login(user) {
  try {
    const { data } = await http.add(loginEnPoint, user);
    return data;
  } catch (ex) {
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
    }
    return null;
  }
}

export async function getUser(id) {
  try {
    const { data } = await http.get(apiEndPoint + id);
    return data;
  } catch (ex) {
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
      return null;
    }
  }
}

export async function updataUser(id, user) {
  try {
    const { data } = await http.update(apiEndPoint + id, user);
    return data;
  } catch (ex) {
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
    }
    return null;
  }
}
