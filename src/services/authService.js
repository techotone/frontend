import jwt_decode from "jwt-decode";

const tokenTag = "secura-token";

export function saveToken(token) {
  localStorage.setItem(tokenTag, token);
}

export function getToken() {
  return localStorage.getItem(tokenTag);
}

export function deleteToken() {
  localStorage.removeItem(tokenTag);
  console.log("token deleted");
}

export function getUserFromToken() {
  let user;
  const token = localStorage.getItem(tokenTag);
  if (token) {
    user = jwt_decode(token);
    delete user.iat;
    delete user.exp;
  }

  return user;
}

export function getUserRole() {
  let user = getUserFromToken();
  if (user) {
    let { role } = user;
    role = role.toLowerCase();
    if (role === "administrator") user.isAdmin = true;
    else if (role === "key manager") user.isKeyManager = true;
    else if (role === "operator") user.isOperator = true;
  }

  return user;
}

export default {
  saveToken,
  getUser: getUserFromToken,
  getToken,
  deleteToken,
};
