import http from "./http";

import config from "../config.json";
import { alertOnStatus, getBaseApi } from "./../utils/utils";

const apiEndPoint = getBaseApi() + "/device/";

export async function getDevices() {
  try {
    const { data } = await http.get(apiEndPoint);
    return data;
  } catch (ex) {
    const { status } = ex.response;
    alertOnStatus(status, ex.response.data);
    return null;
  }
}

export async function deleteDevice(id) {
  try {
    await http.remove(apiEndPoint + id);
    return 0;
  } catch (ex) {
    const { status } = ex.response;
    alertOnStatus(status, ex.response.data);
    return 1;
  }
}

export async function addDevice(bank) {
  try {
    const { data } = await http.add(apiEndPoint, bank);
    return data;
  } catch (ex) {
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
    }
    return null;
  }
}

export async function getDevice(id) {
  try {
    const { data } = await http.get(apiEndPoint + id);
    return data;
  } catch (ex) {
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
    }
  }
}

export async function updateDevice(id, device) {
  try {
    const { data } = await http.update(apiEndPoint + id, device);
    return data;
  } catch (ex) {
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
    }
    return null;
  }
}
