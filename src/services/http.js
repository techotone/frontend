import axios from "axios";

//install interceptor
axios.interceptors.response.use(null, (error) => {
  //ignore client errors
  const clientError =
    (error.response && error.response.status >= 400) || error.response.status < 500;

  if (!clientError) console.log("Client Error", error);

  return Promise.reject(error);
});

export function setAuthToken(token) {
  if (token) axios.defaults.headers.common["x-auth-token"] = token;
}

export function get(uri) {
  return axios.get(uri);
}

export function remove(uri) {
  return axios.delete(uri);
}

export function add(uri, object) {
  return axios.post(uri, object);
}

export function update(uri, object) {
  return axios.put(uri, object);
}

export default {
  get,
  remove,
  add,
  update,
  setAuthToken,
};
