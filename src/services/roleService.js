const roles = [
  { id: 1, role: "Administrator" },
  { id: 2, role: "Key Manager" },
  { id: 3, role: "Operator" },
];

export function getRoles() {
  return roles;
}
