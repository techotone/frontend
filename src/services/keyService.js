import http from "./http";
import config from "../config.json";
import { getBaseApi } from "../utils/utils";
import { alertOnStatus } from "./../utils/utils";

const apiEndPoint = getBaseApi() + "/key/";

export async function getKeys() {
  try {
    const { data } = await http.get(apiEndPoint);
    return data;
  } catch (ex) {
    const { status } = ex.response;
    alertOnStatus(status, ex.response.data);
  }
  return null;
}

export async function deleteKey(id) {
  try {
    await http.remove(apiEndPoint + id);
    return 0;
  } catch (ex) {
    const { status } = ex.response;
    alertOnStatus(status, ex.response.data);
    return 1;
  }
}

export async function addKey(key) {
  try {
    const { data } = await http.add(apiEndPoint, key);
    return data;
  } catch (ex) {
    console.log(ex.response);
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
    }
    return null;
  }
}

export async function getKey(id) {
  try {
    const { data } = await http.get(apiEndPoint + id);
    return data;
  } catch (ex) {
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
      return null;
    }
  }
}

export async function updateKey(id, key) {
  try {
    const { data } = await http.update(apiEndPoint + id, key);
    return data;
  } catch (ex) {
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
    }
    return null;
  }
}
