import http from "./http";
import { alertOnStatus, getBaseApi } from "./../utils/utils";

const apiEndPoint = getBaseApi() + "/bank/";

export async function getBanks() {
  try {
    const { data } = await http.get(apiEndPoint);
    return data;
  } catch (ex) {
    const { status } = ex.response;
    alertOnStatus(status, ex.response.data);

    return null;
  }
}

export async function deleteBank(id) {
  try {
    await http.remove(apiEndPoint + id);
    return 0;
  } catch (ex) {
    const { status } = ex.response;
    alertOnStatus(status, ex.response.data);
    return 1;
  }
}

export async function addBank(bank) {
  try {
    const { data } = await http.add(apiEndPoint, bank);
    return data;
  } catch (ex) {
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
    }
    return null;
  }
}

export async function getBank(id) {
  try {
    const { data } = await http.get(apiEndPoint + id);
    return data;
  } catch (ex) {
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
      return null;
    }
  }
}

export async function updateBank(id, bank) {
  try {
    const { data } = await http.update(apiEndPoint + id, bank);
    return data;
  } catch (ex) {
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
    }
    return null;
  }
}
