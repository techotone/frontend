import http from "./http";

import config from "../config.json";
import { alertOnStatus, getBaseApi } from "./../utils/utils";
const apiEndPoint = getBaseApi() + "/vendor/";

export async function getVendors() {
  try {
    const { data } = await http.get(apiEndPoint);
    return data;
  } catch (ex) {
    const { status } = ex.response;
    alertOnStatus(status, ex.response.data);
    return null;
  }
}

export async function deleteVendor(id) {
  try {
    await http.remove(apiEndPoint + id);
    return 0;
  } catch (ex) {
    const { status } = ex.response;
    alertOnStatus(status, ex.response.data);
    return 1;
  }
}

export async function addVendor(vendor) {
  try {
    const { data } = await http.add(apiEndPoint, vendor);
    return data;
  } catch (ex) {
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
    }
    return null;
  }
}

export async function getVendor(id) {
  try {
    const { data } = await http.get(apiEndPoint + id);
    return data;
  } catch (ex) {
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
      return null;
    }
  }
}

export async function updateVendor(id, vendor) {
  try {
    const { data } = await http.update(apiEndPoint + id, vendor);
    return data;
  } catch (ex) {
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
    }
    return null;
  }
}
