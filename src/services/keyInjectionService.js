import http from "./http";
import { alertOnStatus, getBaseApi } from "../utils/utils";

const apiEndPoint = getBaseApi() + "/keyinjection/";

export async function getConnectedDevices() {
  try {
    const { data } = await http.get(apiEndPoint + "connected");
    return data;
  } catch (ex) {
    const { status } = ex.response;
    alertOnStatus(status, ex.response.data);

    return null;
  }
}

export async function injectKeys(deviceArray) {
  try {
    const { data } = await http.add(apiEndPoint + "inject", deviceArray);
    return data;
  } catch (ex) {
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
    }
    return null;
  }
}

export async function getKeyInjections() {
  try {
    const { data } = await http.get(apiEndPoint);
    return data;
  } catch (ex) {
    if (ex.response) {
      const { status } = ex.response;
      alertOnStatus(status, ex.response.data);
    }
    return null;
  }
}
