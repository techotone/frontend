FROM node:14-alpine
WORKDIR /app
COPY package*.json ./
RUN npm install
EXPOSE 3000
COPY . .
CMD ["npm", "run", "start"]
# CMD ["sh","-c","npm uninstall bcrypt && npm install bcrypt && npm run start"]